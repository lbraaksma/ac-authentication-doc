#######################
Data View Management UI
#######################

The |project| UI shows a list of all registered data views.

.. figure:: /img/auth-dataviewsmanagement.png
   :alt: Dataviews Management Page
   :align: center

   Dataviews Management Page

In this view there are three columns:

 - Data permission name
 - Data permission description
 - Actions

You can filter on name and description.
Data views are administered by the Business Domain Model Service. In current architecture however,
the views are not taken from the Business Domain. Instead, the authentication services reads the views from a file located on the file system.
For the future, the product will ensure that views in the |project| are aligned with the views in the Business Domain Model.
As part of the installation process, the customer will have to ensure that the data views listed in the file match with the data views from the Business Domain Model.
The user with "Administration" rights cannot add or update data views.

****************
Delete Data View
****************

You can delete an existing data view. This will delete the data view from the |project| and not from the Business Domain Model.
Selects the data view to delete and click on the "Dust bin" button.
The |project| will check if the data view is associated with any role.
If this is the case an error message will appear telling in which roles the data views are used. The data view will not be deleted.
If however the data view is not used by any role, the data view is removed from the authentication service

**Note**  If a data view is deleted but it is still listed in the configuration file, it will be added again by the authentication service on startup.
