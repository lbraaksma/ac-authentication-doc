##########################
AC Web Service Permissions
##########################

This pages describes how user permissions are setup for the AC Web Services.
The following Services are described in this chapter:
 - AC Data Browsing Service
-  AC Ops 360 Operations Service and Cleansing
-  AC Select
 - |project| UI

****************************************
Permissions for AC Data Browsing Service
****************************************

The permissions for the AC Data Browsing Service Control access for creating, reading, updating and deleting data and meta data.
AC Data Browsing Service uses the Web UI and Business Domain Model service.
For the Web UI read and write access to data is controlled through data views.
The user can see only data for the classes and the attributes that are included in the data view.
An user has write access to classes and attributes controlled through the 'writable' flag defined in the data view.
If an attribute is writable, an user can change the attribute.
If the class is writable, the user can change ADOs that belong to this class.
The access to data views is configured in the |project| through Roles.

Data access can further be controlled through Business Domain Service (BDMS) application rights.

.. tabularcolumns:: |X|X[4]|
.. Permissions:
.. list-table:: Permissions for Business Domain Service
   :header-rows: 1
   :class: small

   * - Permission
     - Description

   * - BDMS.ALL
     - Full access to BDMS service

   * - BDMS.DATA.ALL
     - Full access to BDMS data services, (eg. read and write)

   * - BDMS.DATA.READ
     - Access to BDMS services for retrieving data

   * - BDMS.META.ALL
     - Full access to BDMS metadata services, eg read and write for classes and attributes

   * - BDMS.META.READ
     - Access to BDMS services for retrieving metadata

**Note** There are currently no methods for writing meta data.
The BDMS.META.ALL is only required when the BDMS service will have methods for creating, updating and deleting classes and attributes.



**********************************************************
Permissions for AC Operations 360 Service and Cleansing UI
**********************************************************

The permissions for these Services control access to the Process Dash board. This dash board is used for monitoring Acquisition,
Derivation and Enrichment, Distribution and Cleansing processes and for actual Data Cleansing (aka Data Processing).

The columns in the table below list the items in the AC Web UI that are accessible from the Operations menu.
The rows lists the operations application permissions.
They map to the AC Web UI menu items and control what the user can access.

.. tabularcolumns:: |X[2]|X[4]|X[2]|X|X|X|X|
.. Permissions:
.. list-table:: Permissions for AC Operations 360 Service and Cleansing UI
   :header-rows: 1
   :class: small

   * - Permissions
     - Description
     - Ops Dashboard
     - Ops Acquisition
     - Ops Derivation
     - Ops Distribution
     - Ops Cleansing

   * - OPS.ALL
     - Access to system information in Ops360
     - X
     - X
     - X
     - X
     - X

   * - OPS.ACQUISITION
     - Access to Acquisition in Ops360
     -
     - X
     -
     -
     -

   * - OPS.DERIVATION
     - Access to Derivation in Ops360
     -
     -
     - X
     -
     -

   * - OPS.DISTRIBUTION
     - Access to Distribution in Ops360
     -
     -
     -
     - X
     -

   * - OPS.CLEANSING
     - Access to Cleansing in Ops360
     -
     -
     -
     -
     - X

   * - OPS.SYSTEM
     -
     -
     -
     -
     -
     -



For accessing the Cleansing UI you need OPS.CLEANSING permission.
What you can see within the Data Cleansing module depends on which BPM you have access to.
An user is allowed to see the states that are defined in the BPM.
If a user does not have access to a BPM for a view, the user will not see any suspects.
The user is allowed to transition data (eg. update the status of a value) as defined in the BPM.
Making transitions outside of the BPM must be prohibited.
In addition:
 - User needs to have permissions to the data view to which the BPM applies.
 - To update a suspect attribute value, the data view needs to have 'writable' enabled for the class and attribute
 - User needs DCIS.GROUPS and DCIS.ISSUES (or DCIS.ALL) to be able to retrieve suspects from the Data Cleansing Issue Service. See below.

Additional application permissions are available on the back-end.

.. tabularcolumns:: |X[6]|X[4]|X[10]|
.. Permissions:
.. list-table:: Permissions for Business Domain Service
   :header-rows: 1
   :class: small

   * - Permission
     - Description
     - Explanation

   * - OPS.PROCESSTRACKING.UPDATE
     - Permission to send updates for processes.
     - Services like AC Connect and Interface Engine needs permission to send process updates to the Process Tracking service.

   * - DCIS.ALL
     - Full access to DCIS service.
     - Cleansing UI uses the Data Cleansing Issue Service for retrieving suspects and folders.

   * - DCIS.GROUPS
     - Access to DCIS for retrieving groups info.
     -

   * - DCIS.ISSUES
     - Access to DCIS for retrieving issues data.
     -

   * - DCIS.CONFIG
     - Create private data cleansing configuration. Can share configurations.
     -

   * - DCIS.ADMIN
     - Can update and delete all configurations. Can make configurations public.
     -


*************************
Permissions for AC Select
*************************

AC Select has the following permissions:
 - ACSELECT.ALL.READ
 - ACSELECT.ALL.WRITE

These permissions need to be updated.
With "ACSELECT.ALL" you have full access to AC Select. With this permission you can create, update and submit a request.
There is also the permission "ACSELECT.READ".



****************************
Permissions for |project| UI
****************************

The table below shows how access to the User Admin UI is controlled.
The rows list the application permissions available for the |project| UI.
The columns in the table below list the sections available in the |project| UI.

.. tabularcolumns:: |X[4]|X[4]|X|X|X|X|X[2]|
.. Permissions:
.. list-table:: Permissions for AC Operations 360 Service and Cleansing UI
   :header-rows: 1
   :class: small

   * - Permissions
     - Description
     - Users
     - Roles
     - Data Views
     - Applications
     - Business Process Model

   * - AUTHENTICATION.ALL
     - Full access to Authentication & Authorization service
     - X
     - X
     - X
     - X
     - X

   * - AUTHENTICATION.USERS
     - Access to Users API for Authentication & Authorization service
     - X
     -
     -
     -
     -

   * - AUTHENTICATION.ROLES
     - Access to Roles API for Authentication & Authorization service
     -
     - X
     -
     -
     -

   * - AUTHENTICATION.DATAVIEWS
     - Access to Dataviews API for Authentication & Authorization service
     -
     -
     - X
     -
     -

   * - AUTHENTICATION.SERVICES
     - Access to Services API for Authentication & Authorization service
     -
     -
     -
     - X
     -

   * - AUTHENTICATION.BPM
     - Access to BPM API for Authentication & Authorization service
     -
     -
     -
     -
     - X

