######################
User Administration UI
######################

You can access the User Administration UI:

 -	through an URL, like ``{BASE-URL}/auth/users``.
 -  by navigating from the top menu bar

.. figure:: /img/auth-user-management-button01.png
   :alt: User Management Button
   :align: center

   User Management Button

This item is only visible if the user has the rights to access the User Administration UI.
The landing page shows all users.

.. figure:: /img/auth-users-management-main01.png
   :alt: User Management Main page
   :align: center

   User Management Main page


On the left there is a menu which includes the following items:

 -	Users - opens the User Administration
 -	Roles - opens the Role Administration
 -	Applications (AC Web Services)
 -	Data views
 -  Business Process Management

The rest of the page is filled with a list of registered users. This is a table with columns for:

 -	User ID
 -	User name
 -	Names of the roles that are assigned to the user. For each role there is a box with the role name and a 'X' icon.
 -	Control buttons
    * Edit user
    * Delete user
    * Change password

The list of user is by default sorted on user ID.
You can also can sort on user ID and user name, Active and LDAP. However it is not possible to sort on role.
There is one filter text box available which can be used to filter on user ID, name or Role.


***************
Create new user
***************

On the top right of the table there is a button the user can select to create a new user.
As soon as a user with *admin* rights selects the "create new user" button, a pop-up pane will appear.

.. figure:: /img/aut-users-management-create.png
   :alt: User Management Create pane
   :align: center

   User Management Create pane


You now can provide a user ID. The |project| will check if there is already a user with the same user ID.
If this is the case an error will appear. If not you can provide the name of the user and assign roles.
If LDAP is enabled on back-end, you can specify if user must be authenticated using LDAP.


*****************
Edit user details
*****************

To edit user details, you can select the "Edit user" control button.
The UI will open a pop-up pane where the user can change userid, user name and roles.

.. figure:: /img/auth-users-management-edit.png
   :alt: User Management Edit pane
   :align: center

   User Management Edit pane

If you want to (de-)select a role:

 - All available roles are included in the "Edit user" pane.
 - On top there is a filter box. You can type a text in the filter box to find a role. Only the roles are shown that match the search string (match on any position in the string).
 - You can scroll through the list of roles.
 - You can click the check box that is in front of the role to select or de-select the role.

***************
Change password
***************

An user with "Administration" rights can generate new passwords in the |project| UI as follows:

 - select the password icon for the user
 - A pop-up appears to confirm
 - A new password is generated automatically for the selected user and copied to the clip-board

The new password needs to be send to the user. This is done outside |project|.
Once the user has logged in with the new password, he/she can change it.

Any user can change his/her password.
The top bar in the AC Web UI (the bar with the AC logo, menu and logout) contains a change password icon:

 - The user selects the change password icon
 - A pop-up window appears where the user can change the password.

Password strength can be checked, if this is enabled passwords must have:

 - at least six characters (can be more)
 - at least one number of symbol (@, #, $, %, etc.)
 - at least one upper case letter
 - at least one lower case letter

If any of these checks fail, an error message will be shown.


************
Disable user
************

Users can be disabled. The user will still exist in the system but is not able to login anymore.
There is a check-box to disable/enable a user. Only administrators who have permission to administer users can enabled/disable users.
A user cannot disable himself. Disabled users are still shown in the users table.
Data permissions, application permissions and LDAP are not shown for disabled users
The user ID and name of a disabled user is light grey to distinguish them from normal users
Column Active is displayed with Yes / No to indicate if the user is active.
You can search on disabled users by typing the text "Disabled"

***********
Delete user
***********

An user with "Administration" rights can delete users by selecting the "Delete user" icon.
You cannot delete your own user ID.

******************
Default admin user
******************

The |project| creates always one admin user that has all rights.