############################
Active Directory Integration
############################

*********************************
Configuration of Active Directory
*********************************

An user with "Administration" rights can configure |project| against Active Directory (AD).
If this is enabled, users can be authenticated using Active Directory (AD) user name and password.
Communication to AD is done through LDAP protocol.

Within the |project| configuration you can enable AD, see Administration Guide.
Additional configuration details need to be provided such as where the AD can be reached.

*************************************
Configuring Active Directory per user
*************************************

It is still required to create a user in the |project|. As an user with "Administration" rights you have to make sure that
the user ID matches with the user ID in the AD administration. Per user it is possible to configure if authentication is done against AD or by the |project|.
When AD is used, password updates will be disabled in the |project| and UI.
AD is only used for authentication. Permissioning such as the configuration of roles is done within |project|.

If authentication is successful when using AD the |project| will return a cookie to the client.
If the authentication is not successful, the |project| will return an error with a message telling why the authentication failed.
The following examples show why authentication can fail:

 - User ID doesn't exist in AD
 - User ID exists but password is incorrect
 - User ID exists but password has expired
 - AD does not respond