##########################
Applications Management UI
##########################

Permissions on AC Web Services are called Applications in the |project| UI.
Each product component (AC Web Service) has its  permissions.
Ideally, each component is in sync with  |project| on which permissions are available for each Application.
In current architecture this is not the case. The authentication service will load in the AC Web Services from a file on the file system.
This will be part of the installation process.
Customer will have to ensure that the services listed in the file match with the permissions from the installed services.

The |project| UI shows a list of all registered Applications.

.. figure:: /img/auth-applications-management.png
   :alt: Applications Management Page
   :align: center

   Applications Management Page

In this view there are three columns:

 - Application name
 - Application description
 - Actions

You can filter on name and description.

The user with "Administration" rights cannot add or update Applications.

******************
Delete Application
******************


An user with "Administration" rights can delete an Application from |project|.
You can selects the Application you want to delete.
The |project| will check if the Application is associated with any role.
If this is the case, an error message will appear showing in which roles the permissions are used.
The Application is not deleted.
If the Application is not assigned to a role the Application (AC Webservcie permission) is removed from the |project|.

*Note* If a permission is deleted but still listed in the configuration file, it will be added again by the |project| on startup.


