######################
Role Administration UI
######################

When opening the Role Administration UI (Click on Roles on the left pane), a list of all available roles is shown.

.. figure:: /img/auth-role-management.png
   :alt: Role Management Page
   :align: center

   Role Management Page


In this list you an find columns for:

 -	Name
 -	Description
 -	Application Permission selected for the role.
 -	Data Permission selected for the role.
 -	Control buttons for:

    * Edit role
	* Delete role

By default the list is sorted by Name. But you can also sort on the other columns.

***************
Create new role
***************

On the top of the table there is a button which you can select to create a new role.

.. figure:: /img/auth-role-management-create.png
   :alt: Role Management Create Pane
   :align: center

   Role Management Create Pane

An Edit pop-up pane appears where you can fill in the details of the new role.
You have to provide a Name and Description and confirm.
It will be checked if there is already a role with the same Name. If this is the case, an error message will appear.
Otherwise the new role will be added.
The user with "Administration" rights can now edit the data views and services for the new role.

*********
Edit role
*********

Name and Description can be edited by selecting the Edit role control button.
It will open a similar popup pane like in the "Create pop-up pane".
You can edit Name, Description, Application Permission and Data Views.
You can also (de)selecting a Application Permission or data view.
The UI will show a pop-up window with names of all available Application Permissions and data views sorted alphabetically.
On top there is a filter box. You can scroll through the list of names.
You can also type a text in the filter box to find a role.
If the role is found you can (un)check to (de)select a role or data view.







