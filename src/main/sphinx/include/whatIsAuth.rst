##################
What is |project|?
##################

|project| belongs to the new collection of Asset Control Services such as Data Cleansing, Data Browsing and the Core UI Service.
They are developed based on the MicroService architecture.

|project| is a system controlling authentication and authorization of users.
It introduces several concepts that are shared with other applications.
These concepts are: *Service*, *Role* and *Right*.

Each application has some actions that users can be authorized or not authorized to perform.
Such actions are called *Services* in |project| terminology.
Each *Service* has a string representation(*a name*) and a description that explains what this service represents.

One or more *Roles* can be assigned to an user. Every *Role* consists of zero or more *DataViews* and zero or more  *Services*.
So when a *Role* is assigned to a user, the user has been authorized to use specific *DataViews* and *Services*.


In order to control these authorizations and avoid constant data exchange between |project| and applications concept of a *Right* was introduced.
*Right* is a signed token bound to a specific user and containing service name that can be passed along with any request and verify that this particular user has a right to perform this particular action.

.. figure:: /img/autho_and_authe_model01.png
   :alt: Auth Model: User, Role, Service, Right
   :align: center

   Authorisation and Authentication Model: User, Role, Service, Right

#################
How does it work?
#################

|project| will for verification of the tokens check if each *Right* has a *signature* field that contains cryptographically signed hash of all the other fields.

To verify a *Right* the client application can implement it's own verification or use a package provided by |project|.

Cryptographic functions used by |project| are based on private/public key pairs contained in Java keystore,
so all the applications have to share a specific keystore and specific key pair in order to use verification.


*************
Main concepts
*************

.. _ag-user:

====
User
====

User can have any number of roles.

User consists of:
    * user ID (*unique*)
    * name
    * password

.. _ag-role:

====
Role
====

Role is a collection of Services and Dataviews.

Role consists of:

    * name (*mandatory*, *unique*)
    * description (*optional*)

.. _ag-service:

=======
Service
=======

Each Service represents some action that users are allowed to perform on some specific AC service.
In case of |project| users can have rights to manipulate users, roles, services or dataviews.

Service consists of:

    * name (*mandatory*, *unique*)
    * description (*optional*)

.. _ag-dataview:

========
Dataview
========

Each Dataview represents a specific view on some datamodel that users are allowed to see.

Dataviews have names(*mandatory*) and descriptions(*optional*).

Dataview consists of:

    * name (*mandatory*, *unique*)
    * description (*optional*)

.. _ag-right:

**************
Authentication
**************

When user authenticates with |project|, they will receive a cookie.
This cookie is a Base64 encoded array of Right objects.

Each Right object represents a particular *right* a user has for either executing an action(represented by name of the :ref:`ag-service`)
or viewing particular data(represented by name of the :ref:`ag-dataview`).
Right consists of:

    * user ID
    * service or dataview name
    * time of issue
    * signature

The cookie will be passed along with all requests to any other AC Web Services.
These Services should identify the Rights that they are interested in, verify their signatures and *optionally* check their expiration.

******************
Default user admin
******************

By default an administrative user with username/password **admin/admin** is created.
The password for this user is marked as expired so |project| won't accept it until it is changed.
This Administrative user (admin) can be used to create other users and their roles with corresponding services and dataviews.

.. figure:: /img/admin_model01.png
   :alt: Admin Model
   :align: center

   Admin User Model

##################
|project| REST API
##################


|project| provides REST API for some of the following manipulations.

.. figure:: /img/Auth_REST_API_01.png
   :alt: Auth REST API
   :align: center

   Authorisation and Authentication REST API


Schema of this API is provided in swagger format at **<host>:<port>/swagger-ui** endpoint.
The same schema can be obtained as a file directly at **<host>:<port>/swagger.yaml**.
For example with the following URL all possible REST API calls can be retrieved:

``http://[servername]:9005/auth/lib/swagger-ui/index.html?url=/auth/swagger.yaml#/``.

You will see the following screen:

.. figure:: /img/auth-swagger-ui-main.png
   :alt: Swagger REST API
   :align: center

   Swagger UI REST API

When you click on for instance the "POST /users" it will show how to use this URL and the response you can expect.

.. figure:: /img/auth-swagger-ui-users.png
   :alt: Swagger REST API Users POST operation.
   :align: center

   Swagger UI REST API Users POST operation.


############################
|project| Web UI
############################

The AC Web UI (which can include services like the Operations 360 Dashboard and Browsing UI) has been extended for managing the user accounts.
|project| Web UI includes:

**Administration of Users**

 - Show users and user details
 - Create/delete user
 - Edit user: user ID, name, assigned roles
 - Change password

**Administration of roles**

 - Show roles and role details
 - Create/delete role
 - Edit role: role ID, name, assigned services, assigned data views

The |project| also includes a registration of data views and services. These are managed by the system:

 - The data view name within |project| must match the data view name from the Business Domain Model.
 - The various services (eg. Business Domain Model, Data Browsing UI) control the services administered in the authentication service.

The |project| shows the available data views and services for configuring a role.
It is not possible to create, update, delete a data view or service from the |project| Web UI

This Web UI will be described in detail in the |project| User Guide.

############################
AC MicroService Architecture
############################

A MicroServices architecture is a collection of independently deploy-able services.
Every service has its own function and will be deployed in its own Docker container.
Every container has its own isolated workload environment making it independently deploy-able and scalable.
Each individual AC Service runs in its own isolated environment, separate from the others within the architecture.
Some of the new AC Services are Web applications having their own Web User Interfaces (UI). They are developed to
provide the the ability to configure a single unified Web UI (AC Web UI).
The AC Core UI Service is the core component of the AC Services representing the common properties of this UI.
