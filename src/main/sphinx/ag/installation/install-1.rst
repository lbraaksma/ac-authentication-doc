*****************************
Minimal Software Requirements
*****************************

* Docker
* Proxy service (e.g. a separate NGNIX container)
* AC Server 6.3 (or higher)

All software requirements must be met prior to installing the |project| . If not, please resolve this first.

******************************
Installing the |project|
******************************

The |project| is provided as a Docker image file which you can download from the Asset Control Userweb as follows:

   - Log onto the Asset Control `Userweb <https://services.asset-control.com/userweb>`_. Login
     credentials are required to access this site.
   - In the “AC Plus Applications” section, follow the link for “AC AUTH".
   - Click the link in the "Application builds" section to download the package file.

The docker image file for |project| is contained in a `*.tar.gz` archive.
There is no need to unzip the package just load the Docker image file to your local Docker repository.
For example let's say the image name is authentication-service:1.0.0 you can use the command:


.. code-block:: console

   docker load <authentication-service-1.0.0-imagefile>.tar.gz

In this Administration Guide we will use the docker-compose utility.

|project| loads information about Business Process Monitor Service, Applications (Permissons on AC Webservcies) and data views on startup.
The files need to be put in the folder ``/opt/docker/conf/load`` inside the |project| container.
In this Administration Guide this directory will be bind mounted to a directory (volume) on the host machine.
In our docker-compose example a volume ``/myhost/docker-volumes/auth/load`` has been created and mounted to the folder ``/opt/docker/conf/load`` of the |project| container.
So the files can be put in this volume.
This is described in the "Configuration" chapter.

With the ``volumes`` parameter in a docker-compose file you can do this mount.
A docker-compose file,  :file:`docker-compose.yml` would look like:

.. code-block:: console

   auth-service:
     image: "localdockerrepo/authentication-service:1.0.0"
     container_name: auth-service
     hostname: auth-service
     expose:
       - "9000"
     environment:
       - "PASSWORD_STRENGTH_CHECK_ENABLED=true"
       - "LDAP_ENABLED=true"
       - "LDAP_URL=ldap://123.45.67.89:123"
       - "LDAP_SECURITY_PRINCIPAL=$$userName@sso.prod"
     volumes:
       - "/myhost/docker-volumes/auth/external:/opt/docker/conf/external:ro"
       - "/myhost/docker-volumes/auth/DB:/opt/docker/DB:Z"
       - "/myhost/docker-volumes/auth/load:/opt/docker/conf/load"
     networks:
       - ac-net
     command: -Dlogger.file=/opt/docker/conf/external/logback.xml -Dconfig.file=/opt/docker/conf/external/docker.conf


When |project| is started it will create a database inside the container.
To avoid this database will be destroyed when the container is stopped you can use an external volume.
This docker-compose file declares a local folder on the host ``/myhost/docker-volumes/auth/DB`` to be a Docker volume that is bind mounted to the folder ``/opt/docker/DB`` inside the container.
The |project| DataBase can be added to this volume.

There is also a volume ``/myhost/docker-volumes/auth/external`` for the |project| keystore.
Every other AC Webservice needs to add its public key to this keystore so the |project| an these AC Webservices can communicate.
You can read more on to add keys on the following website: ``https://docs.oracle.com/javase/tutorial/security/toolsign/step3.html`` for example.

On this volume in this example also a ``docker.conf`` file has been place.
This file can be used to override application properties.
These properties are described in the "Configuration" chapter.

**Note** first line of this file needs to be "include application.conf"

For the use of LDAP some environment variables have been added to this Docker compose file.
This will be described in the "Configuration" chapter.

In this example containers are configured to use a virtual docker `ac-net` network.
Port 9000 will be exposed to access the |project| outside the `ac-net` network.

********************************
Upgrading the |project|
********************************

Stop and remove the container (when using docker-compose) but do not remove the volume.
Run the following command from the folder where your docker-compose file resides:

.. code-block:: console

   docker-compose down


Download the new image from `Userweb <https://services.asset-control.com/userweb>`.
Load the new image to your local Docker repository.
Update the docker-compose file with the new image.


********************************
Uninstall the |project| Service
********************************

To stop and remove the container and remove volume (when using docker-compose).
Run the following command from the folder where your docker-compose file resides:


.. code-block:: console

   docker-compose down --volumes


It is not common practice to remove the image as the old image can be kept without problem.
Look for the `image id`  with command:

.. code-block:: console

   docker images

However if you wish to remove the image the command is:


.. code-block:: console

   rmi imageid

If you have made changes to the configuration file of a proxy server for the |project| do not forget to delete them as well.
